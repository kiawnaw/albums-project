

# Albums app - Kiana Karamouz

## Overview

This app uses cookies for the last visit date and time. It also uses sessions for the administration part.Moreover, it does crud operations for a list of albums and studios. 

## Technologies Used

This app is built on:

* ASP.NET
 

## Getting Started

You will need:
* Visual Studio 2019
* NuGet packages


### To run the app locally

click on the final-exam.sln file and then run the code

**For more information refer to the license file.
BSD licenses are a low restriction type of license for open source software that does not put requirements on redistribution
